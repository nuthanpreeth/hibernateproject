package org.dxc;

import org.dxc.entity.Employee;
import org.hibernate.*;
import org.hibernate.cfg.Configuration;

class App 
{
	private static SessionFactory factory;
    public static void main( String[] args )
    {
        try {
        	//factory Object Created
        	factory=new Configuration().configure().buildSessionFactory();
        } catch(Throwable e) {
        	System.err.println("Failed to create Session Instance");
        	throw new ExceptionInInitializerError(e);
        }
        
        App employe=new App();
        
        /*Add Employee Details */
        Integer empID1=employe.addEmployee("Nuthan","Preeth",1000);
        Integer empID2=employe.addEmployee("Rudresh","Aradya",5000);
        Integer empID3=employe.addEmployee("Akhil","Banagiri",3000);
    }
    
    public Integer addEmployee(String fname,String lname,int salary) {
    	//session object to be extracted from factory
    	Session session=factory.openSession();
    	Transaction tx=null;
    	Integer employeeID=null;
    	
    	try {
    		tx=session.beginTransaction();
    		//Object Model is ready
    		Employee employee = new Employee(fname, lname, salary);
    		//add it to relational model
    		employeeID=(Integer) session.save(employee);
    		tx.commit();
    		System.out.println("Employee Record Created Successfully...");
    	}catch(HibernateException e) {
    		if(tx!=null)
    	    tx.rollback();
    		e.printStackTrace();
    	} finally {
    		session.close();
    	}
    	return employeeID;
    }
}
